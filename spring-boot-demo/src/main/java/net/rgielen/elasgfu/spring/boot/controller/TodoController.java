package net.rgielen.elasgfu.spring.boot.controller;

import net.rgielen.elasgfu.spring.boot.repository.TodoItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import net.rgielen.elasgfu.spring.boot.entity.TodoItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@RestController
@RequestMapping(path = "/todos")
public class TodoController {

    @Autowired
    TodoItemRepository todoItemRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<TodoItem> litsTodos() {
        return todoItemRepository.findAll();
    }

    @RequestMapping(path = "/fakes", method = RequestMethod.GET)
    public List<TodoItem> litsFakeTodos() {
        final List<TodoItem> fakes = new ArrayList<>();
        fakes.add(new TodoItem("foo", "bar"));
        fakes.add(new TodoItem("foo", "bar"));
        fakes.add(new TodoItem("foo", "bar"));
        fakes.add(new TodoItem("foo", "bar"));
        fakes.add(new TodoItem("foo", "bar"));
        fakes.add(new TodoItem("foo", "bar"));
        return fakes;
    }

    // http://localhost/todos/1
    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public TodoItem findById(@PathVariable Integer id) {
        return todoItemRepository.findOne(id);
    }

}
