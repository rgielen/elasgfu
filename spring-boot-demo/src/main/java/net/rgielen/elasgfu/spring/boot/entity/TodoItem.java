package net.rgielen.elasgfu.spring.boot.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * TodoItem.
 *
 * @author Rene Gielen
 */
@Entity
@SequenceGenerator(name = "todoitem_id_seq", sequenceName = "todoitem_id_seq", allocationSize = 1)
public class TodoItem {

    @Id
    @GeneratedValue(generator = "todoitem_id_seq")
    private Integer id;
    @Temporal(TemporalType.DATE)
    private Date dueDate;
    private String topic;
    @Column(length = 1024)
    private String description;
    private String category;
    private boolean done;
    @ManyToOne
    private Person owner;

    public TodoItem() {
    }

    public TodoItem(String topic, String description) {
        this.topic = topic;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "TodoItem{" +
                "id=" + id +
                ", dueDate=" + dueDate +
                ", topic='" + topic + '\'' +
                ", description='" + description + '\'' +
                ", category='" + category + '\'' +
                ", done=" + done +
                '}';
    }
}
