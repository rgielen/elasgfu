package net.rgielen.elasgfu.todo.repository;

import net.rgielen.elasgfu.todo.TodoConfig;
import net.rgielen.elasgfu.todo.entity.TodoItem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TodoConfig.class)
@Transactional
public class TodoItemRepositoryTest {

    @Autowired
    TodoItemRepository todoItemRepository;

    @Test
    public void testRepositoryIsPresent() throws Exception {
        assertNotNull(todoItemRepository);
    }

    @Test
    public void testCreateAndReadWorks() throws Exception {
        TodoItem todoItem = new TodoItem("foo", "bar");
        TodoItem saved = todoItemRepository.save(todoItem);
        assertNotNull(saved.getId());
        TodoItem loaded = todoItemRepository.findOne(saved.getId());
        loaded.setTopic("blabla");
        todoItemRepository.flush();
        assertNotNull(loaded);
        assertEquals(saved.getId(), loaded.getId());
    }

    @Test
    public void testFindByDueDate() throws Exception {
        TodoItem todoItem = new TodoItem("foo", "bar");
        final Date dueDate = new Date();
        todoItem.setDueDate(dueDate);
        TodoItem saved = todoItemRepository.save(todoItem);
        final List<TodoItem> byDueDate = todoItemRepository.findByDueDate(dueDate);
        assertTrue(byDueDate.size()>0);
    }

}