package net.rgielen.elasgfu.todo.service;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class BarServiceTest {

    @Test
    public void testPlainBarService() throws Exception {
        new BarService().bar();
    }

    @Test
    public void testLogingBarService() throws Exception {
        new TimeLoggingBarService().bar();
    }
}
