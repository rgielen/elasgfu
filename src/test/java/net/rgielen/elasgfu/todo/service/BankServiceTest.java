package net.rgielen.elasgfu.todo.service;

import net.rgielen.elasgfu.todo.TodoConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TodoConfig.class)
public class BankServiceTest {

    @Autowired
    BankService bankService;

    @Test
    public void testBankServiceGetsInjected() throws Exception {
        assertNotNull(bankService);
    }

    @Test
    public void testTransfer() throws Exception {
        bankService.transfer(100d, "123", "345");
    }

    @Test
    public void testTheImportnatStuff() throws Exception {
        bankService.doSomethingImportant();
    }
}