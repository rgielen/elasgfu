package net.rgielen.elasgfu.todo.service;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class BarService {

    public void foo(String bla) {
        // some code
    }

    public Integer bar() {
        return null;
    }

}
