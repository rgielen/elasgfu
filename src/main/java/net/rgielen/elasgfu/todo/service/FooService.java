package net.rgielen.elasgfu.todo.service;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public interface FooService {

    void foo(String bla);
    Integer bar();

}
