package net.rgielen.elasgfu.todo.service;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class TimeLoggingBarService extends BarService {

    @Override
    public void foo(String bla) {
        final long start = System.currentTimeMillis();
        super.foo(bla);
        final long end = System.currentTimeMillis();
        System.out.println(end-start);
    }

    @Override
    public Integer bar() {
        final long start = System.currentTimeMillis();
        final Integer bar = super.bar();
        final long end = System.currentTimeMillis();
        System.out.println(end-start);
        return bar;
    }
}
