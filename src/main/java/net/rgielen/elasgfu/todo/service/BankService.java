package net.rgielen.elasgfu.todo.service;

import net.rgielen.elasgfu.todo.entity.Konto;
import net.rgielen.elasgfu.todo.repository.KontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Component
public class BankService {

    @Autowired
    private KontoRepository kontoRepository;

    /**
     * Transfer when only numbers are known
     */
    @Transactional
    public void transfer(Double amount, String fromNr, String toNr) {
        final Konto from = kontoRepository.findById(fromNr);
        final Konto to = kontoRepository.findById(toNr);
        logIntendedTransfer();
        transfer(amount, from, to);
    }

    /**
     * Transfer when accounts are already given
     */
    @Transactional
    public void transfer(Double amount, Konto from, Konto to) {
        from.setKontostand(from.getKontostand()-amount);
        //-------- hier darf keine Unterbrechung erfolgen für die Persitenz!
        to.setKontostand(to.getKontostand()+amount);
    }


    //-- Some Exception Examples ...

    @Transactional(propagation = REQUIRES_NEW, isolation = Isolation.REPEATABLE_READ)
    public void logIntendedTransfer() {
        try {
            tuNix();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void doSomethingImportant() {
        doSomethingImportantTransactionally();
    }

    @Transactional
    private void doSomethingImportantTransactionally() {
        System.out.println("Hallo");
    }

    public void tuNix() throws IOException {
        throw new IOException("Boom!");
    }

    public void tuWasMitTuNix() {
        try {
            tuNix();
        } catch (IOException e) {
            e.printStackTrace();
            // TODO: Mach was
        }
    }


}
