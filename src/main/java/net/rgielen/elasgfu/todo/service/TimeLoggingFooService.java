package net.rgielen.elasgfu.todo.service;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class TimeLoggingFooService implements FooService {

    final FooService innerFooService;

    public TimeLoggingFooService(FooService innerFooService) {
        this.innerFooService = innerFooService;
    }

    public void foo(String bla) {
        final long start = System.currentTimeMillis();
        innerFooService.foo(bla);
        final long end = System.currentTimeMillis();
        System.out.println(end-start);
    }

    public Integer bar() {
        final long start = System.currentTimeMillis();
        final Integer bar = innerFooService.bar();
        final long end = System.currentTimeMillis();
        System.out.println(end-start);
        return bar;
    }
}
