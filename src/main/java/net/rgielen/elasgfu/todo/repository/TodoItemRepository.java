package net.rgielen.elasgfu.todo.repository;

import net.rgielen.elasgfu.todo.entity.TodoItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public interface TodoItemRepository extends JpaRepository<TodoItem, Integer> {

    List<TodoItem> findByDueDate(Date dueDate);

    TodoItem findFirstByDueDate(Date dueDate);

    @Query("select t from TodoItem t where t.description like ?1")
    List<TodoItem> findByVariousCriteria(String descriptionTokenToFind);

    @Query("select t from TodoItem t where t.owner.id = ?1")
    List<TodoItem> findByPersonId(String personId);

    List<TodoItem> findByOwnerId(String personId);

    @Query("select t from TodoItem t left outer join fetch t.owner o where o.id = ?1")
    List<TodoItem> findByPersonIdAltenative(String personId);


}
