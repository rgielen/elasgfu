package net.rgielen.elasgfu.todo.repository;

import net.rgielen.elasgfu.todo.entity.Konto;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Component
@Transactional
public class KontoRepository {

    @PersistenceContext
    EntityManager em;

    public Konto findById(String id) {
        return em.find(Konto.class, id);
    }
}
